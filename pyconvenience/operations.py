# -*- coding: utf-8 -*-
"""
Convenience functions for operations on objects in memory

Copyright 2018 Tom Brouwer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


def sort_dict_list(list_, key, reverse=False):
    """
    Sort a list of dicts, based on a dict key
    """
    return sorted(list_, key=lambda k: k[key], reverse=reverse)
