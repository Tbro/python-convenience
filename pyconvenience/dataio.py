# -*- coding: utf-8 -*-
"""
Convenience functions for loading and saving data from/to disk

Copyright 2018 Tom Brouwer

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import json
import copy
import gzip
import csv

import yaml


def open_file(fileloc, method, gzipped=False):
    """
    Function to open both gzipped and non gzipped files with utf8 encoding
    """
    if gzipped:
        method2mode = {'w': 'wt', 'a': 'at', 'r': 'rt'}
        mode = method2mode[method]
        return gzip.open(fileloc, mode, encoding='utf8')
    else:
        return open(fileloc, method, encoding='utf8')


class JSONLinesFile():
    """
    JSONLinesFile class, can be used in combination with a with statement, to
    write to a jsonlinesfile

    Arguments:
        fileloc --- str: The file location

        method --- str: The method for reading/writing. Supports 'w' and
        'a'

        gzipped=False --- bool: Whether the file is gzipped or not
    """

    def __init__(self, fileloc, method, gzipped=False):
        self.fd = open_file(fileloc, method, gzipped=gzipped)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.fd.close()

    def write_line(self, dat):
        """
        Write data to a single line of the file

        Arguments:
            dat --- any: A single dict/list/int/float/str to write as a
            jsonline in the file
        """
        self.fd.write(json.dumps(dat, ensure_ascii=False) + '\n')

    def write_lines(self, list_):
        """
        Write data from a list as JSON to multiple lines in the JSONLinesFile
        """
        for item in list_:
            self.write_line(item)


def iterate_jsonlines(in_fileloc, gzipped=False, ignore_errors=False):
    """
    Iterator that returns objects for each line in a json-lines file

    Arguments:
        in_fileloc --- str: The location of the jsonlines file

        gzipped=False --- bool: If the jsonlines file is gzipped, set this to
        true, to read directly from the gzipped file

        ignore_errors=False --- bool: This ignores decode errors, that for
        example occur when there are incomplete lines in a file

    Yields:
        any type: The data from a single jsonlines file line as an object
    """
    with open_file(in_fileloc, 'r', gzipped=gzipped) as jsonlinesfile:
        for line in jsonlinesfile:
            if ignore_errors:
                try:
                    yield json.loads(line)
                except json.JSONDecodeError:
                    continue
            else:
                yield json.loads(line)


def count_lines(in_fileloc, gzipped=False):
    """
    Count the lines in the specified file. WARNING: Each line is loaded into
    memory, meaning OutOfMemory may occur if the file is not devided into lines

    Arguments:
        in_fileloc --- str: The location of the file

        gzipped --- bool: Whether the input file is gzipped

    Returns:
        int --- The number of lines in the file
    """
    count = 0
    with open_file(in_fileloc, 'r', gzipped=gzipped) as linesfile:
        for line in linesfile:
            count += 1
    return count


def process_jsonlines(in_fileloc, out_fileloc, process_function,
                      write_method='w', load_gzipped=False,
                      write_gzipped=False, in_memory=1000,
                      ignore_errors=False, start=0, end=None):
    """
    Processes the entries of the input file, using the 'process_function', and
    writes the results to the output file

    Arguments:
        in_fileloc --- str: The location of the jsonlines file

        out_fileloc --- str: The location to write the output file

        process_function --- function: The function that is executed on every
        line in the data. Input of the function should be a single item from
        the json-lines file (already parsed), and output is the item that will
        be written to the output file. If the function returns None for an
        item, nothing is written to the output file.

        write_method='w' --- str: The method to use for writing the data

        load_gzipped=False --- bool: Whether the input file is gzipped

        write_gzipped=False --- bool: Whether the output file should be written
        gzipped

        in_memory=1000 --- str: The number of results to store in memory,
        before writing them to the output file. Making this higher prevents
        traditional Harddisks from switching between two locations on drive,
        but consumes more memory.

        ignore_errors=False --- bool: This ignores decode errors, that for
        example occur when there are incomplete lines in a file

        start=0 --- int: The line to start processing at

        end=None --- int: The line to end processing at
    """
    save_queue = []
    method = copy.copy(write_method)
    with JSONLinesFile(out_fileloc, method, gzipped=write_gzipped) as\
            outlinesfile:
        for i, item in enumerate(
                        iterate_jsonlines(
                            in_fileloc, gzipped=load_gzipped,
                            ignore_errors=ignore_errors
                            )
                        ):
            if end is not None and i == end:
                break
            elif i < start:
                continue
            processed_item = process_function(item)
            if processed_item is not None:
                save_queue.append(processed_item)

            if len(save_queue) == in_memory:
                outlinesfile.write_lines(save_queue)
                save_queue = []

        outlinesfile.write_lines(save_queue)


def load_processed_jsonlines(in_fileloc, process_function, start=0, end=None,
                             gzipped=False, ignore_errors=False):
    """
    Return the data from a jsonlines file, processed using a processing
    function

    Arguments:
        in_fileloc --- str: The location of the jsonlines file

        process_function --- function: A function that takes a single parsed
        line from the json-lines file, and returns the processed data, or None,
        in which case it is ignored.

        start=0 --- int: The index of the first line to load (count
        from 0)

        end=None --- int: The index of the line that should not be loaded
        anymore

        ignore_errors=False --- bool: This ignores decode errors, that for
        example occur when there are incomplete lines in a file
    """
    data = []
    for ind_, dat in enumerate(iterate_jsonlines(in_fileloc, gzipped=gzipped,
                                                 ignore_errors=ignore_errors)):
        if end is not None and ind_ == end:
            break
        elif ind_ >= start:
            processed = process_function(dat)
            if processed is not None:
                data.append(processed)

    return data


def find_jsonlines_item(in_fileloc, key, value, gzipped=False,
                        ignore_errors=False):
    """
    Find a specific item in a jsonlines file with on each line an object(dict)
    that contains a specific key/value combination.

    Arguments:
        in_fileloc --- str: The location of the input file

        key --- any: Can be a simple string, int or float, but can also be a
        dict, in which case it is interpreted as a nested key.

        value --- any: The value of the given key

        ignore_errors=False --- bool: This ignores decode errors, that for
        example occur when there are incomplete lines in a file
    """
    def get_key_value(data, key):
        if not isinstance(key, dict):
            v = data.get(key)
        else:
            k = next(iter(key))
            next_key = key[k]
            next_data = data.get(k)
            if next_data is not None:
                v = get_key_value(next_data, next_key)
            else:
                v = None
        return v

    for item in iterate_jsonlines(in_fileloc, gzipped=gzipped,
                                  ignore_errors=ignore_errors):
        out_value = get_key_value(item, key)
        if out_value == value:
            return item


def load_jsonlines(in_fileloc, start=0, end=None, gzipped=False,
                   ignore_errors=False):
    """
    Return the data from a jsonlines file

    Arguments:
        in_fileloc --- str: The location of the jsonlines file

        start=0 --- int: The index of the first line to load (count
        from 0)

        end=None --- int: The index of the line that should not be loaded
        anymore

        ignore_errors=False --- bool: This ignores decode errors, that for
        example occur when there are incomplete lines in a file
    """
    data = []
    for ind_, dat in enumerate(iterate_jsonlines(in_fileloc, gzipped=gzipped,
                                                 ignore_errors=ignore_errors)):
        if end is not None and ind_ == end:
            break
        elif ind_ >= start:
            data.append(dat)

    return data


def iterate_delimited_data(in_fileloc, colnames=None, delimiter=',',
                           quotechar='"', dialect='unix'):
    """
    Iterates over delimted data (e.g. CSV) and yields a dict for each line

    Arguments:
        in_fileloc --- str: The location of the input file

        colnames=None --- list: In case only a limited number of columns needs
        to be exported, give their exact names in a list

        delimiter=',' --- See csv.reader

        quotechar='"' --- See csv.reader

        dialect='unix' --- See csv.reader

    Yields:
        dict --- The data of a single line
    """
    with open(in_fileloc, newline='', encoding='utf8') as csvfile:
        csv_reader = csv.reader(csvfile, dialect=dialect,
                                delimiter=delimiter, quotechar=quotechar)
        first_row = True
        for row in csv_reader:
            if first_row:
                headers = row
                first_row = False
                if colnames:
                    map_ind = range(len(headers))
                    relevant_cols = [(h, i) for h, i in zip(headers, map_ind)
                                     if h in colnames]
            else:
                if colnames:
                    dat = {h: row[i] for h, i in relevant_cols}
                else:
                    dat = {h: d for h, d in zip(headers, row)}
                yield dat


def write_lines(list_, file_desc, add_newline=True):
    """
    Write string data from a list as lines to an output file
    """
    for item in list_:
        if add_newline:
            file_desc.write(item + '\n')
        else:
            file_desc.write(item)


def copy_lines_file(input_loc, output_loc, write_method='w', batchsize=10000,
                    load_gzipped=False, write_gzipped=False):
    """
    Copy the raw lines, e.g. in a json-lines file, to another file.

    Arguments:
        input_loc --- str: The file location to read from

        output_loc -- str: The location of the file to write, or append data to

        write_method='w' --- str: The method used for writing. Change this to
        'a' to append to a file

        batchsize=10000 --- int: To prevent continuous switching between to
        locations on the drive (slow for traditional HDDs), set the batchsize
        to determine the number of lines to load into memory before writing.

        load_gzipped=False --- bool: Whether the file being loaded is gzipped

        write_gzipped=False --- bool: Whether the file being saved should be
        gzipped
    """
    line_queue = []
    with open_file(input_loc, 'r', gzipped=load_gzipped) as inputfile:
        with open_file(output_loc, write_method, gzipped=write_gzipped) as\
                output_file:
            lqsize = 0
            for line in inputfile:
                line_queue.append(line)
                lqsize += 1
                if lqsize == batchsize:
                    write_lines(line_queue, output_file, add_newline=False)
                    line_queue = []
                    lqsize = 0
            else:
                write_lines(line_queue, output_file, add_newline=False)


def load_json(in_fileloc):
    """
    Return the data from a JSON file

    Arguments:
        in_fileloc --- str: The location of the json file
    """
    with open(in_fileloc, 'r', encoding='utf8') as jsonfile:
        data = json.load(jsonfile)

    return data


def save_json(data, out_fileloc, **kwargs):
    """
    Save data to a JSON file

    Arguments:
        out_fileloc --- str: The location of the json file
    """
    with open(out_fileloc, 'w', encoding='utf8') as jsonfile:
        json.dump(data, jsonfile, ensure_ascii=False, **kwargs)


def save_jsonlines(list_, out_fileloc, method='w', gzipped=False):
    """
    Save a list to a json-lines file
    """
    with JSONLinesFile(out_fileloc, method, gzipped=gzipped) as jsonlinesfile:
        jsonlinesfile.write_lines(list_)


def load_yaml(in_fileloc):
    """
    Load data (safe load) from a YAML file

    Arguments:
        in_fileloc --- str|pathlib.Path: The path to the yaml file
    """
    with open(in_fileloc, 'r', encoding='utf-8') as yamlfile:
        return yaml.safe_load(yamlfile)


def save_yaml(data, out_fileloc):
    """
    Save data to a YAML file

    Arguments:
        data --- The data to save to the YAML file

        out_fileloc --- str|pathlib.Path: The path to save the YAML file
    """
    with open(out_fileloc, 'w', encoding='utf-8') as yamlfile:
        yaml.dump(data, yamlfile, allow_unicode=True, sort_keys=False)


def load_lines(in_fileloc):
    """
    Loads each line from the given file into a list of strings (UTF-8)

    Args:
        in_fileloc:
            A string or pathlib.Path indicating the location of the file

    Returns:
        A list of strings in which each item corresponds to a specific line.
        Empty lines are not included
    """
    data = []
    with open(in_fileloc, 'r', encoding='utf8') as linesfile:
        for line in linesfile:
            linedata = line.strip()
            if linedata:
                data.append(linedata)

    return data


def save_lines(data, out_fileloc):
    """
    Saves the contents of an iterable of strings to a line seperated file
    (UTF-8)

    Args:
        data:
            Iterable containing strings
        out_fileloc:
            String or pathlib.Path indicating the output file
    """
    with open(out_fileloc, 'w', encoding='utf8') as linesfile:
        for str_ in data:
            linesfile.write(str_ + '\n')
