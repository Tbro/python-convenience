# Python Convenience package
Contains convenient functions for Python 3

This package can be installed by running the following command in the root
directory of this project:

`pip install .`

## Package setup
The package contains the following submodules:

* [dataio](pyconvenience/dataio.py): Contains functions for data in- and output
* [operations](pyconvenience/operations.py): Operations on in-memory objects

## Use examples
The following are some examples of functions from the package:

__Simply loading files into memory__
```python
from pyconvenience import dataio

# Load the first 1000 lines of a json-lines file
first_1000 = dataio.load_jsonlines('inputfile.jsonl', end=1000)

# Load the second 1000 lines of a json-lines file
second_1000 = dataio.load_jsonlines('inputfile.jsonl',start=1000, end=2000)

# NOTE: To process entries, without loading all of them in memory, use
# iterate_jsonlines or process_jsonlines
```

__Iterate through a json-lines file__
```python
from pyconvenience import dataio

for data in dataio.iterate_jsonlines('/home/user/Downloads/file.jsonl'):
    print(data['name'])
```

__Editing/filtering the lines of a json-lines file, and write to new file__
```python
from pyconvenience import dataio

# Create a function used to filter each entry
def filter_invalid_type(entry):
    type = entry.get('type')
    if type is not None or type == 'invalid':
        return None
    else:
        entry['is_valid'] = True
        return entry

# Apply function to each line in file, 'None' results are not written...
dataio.process_jsonlines('input_file.jsonl', 'output_file.jsonl',
                         filter_invalid_type)
```
